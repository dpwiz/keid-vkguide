keid-vkguide
============

This project implements the https://vkguide.dev using the Haskell engine https://keid.haskell-game.dev/ .


## 0. Initial Setup

To compile the code you need the haskell toolchain and vulkan development tools and libraries.

> When using nix, make sure to use nixGL to match your system driver version.
> For example, with intel gpu: `nix develop --command nixVulkanIntel cabal run vkguide-chapter3`

Run the examples using `cabal run keid-vkguide $ModuleName`

For example, run `cabal run keid-vkguide InitialSetup` to validate the toolchain is working.

- [InitialSetup](./src/InitialSetup.hs) defines an empty stage.


## 1. Initialization and render Loop

- [RenderDearImGui](./src/RenderDearImGui.hs) draws a dear imgui window using the render-basic render pass.


## 2. The Graphics Pipeline, Rendering first triangle

- [TriangleRed](./src/TriangleRed.hs) draws a red triangle.
- [TriangleColor](./src/TriangleColor.hs) adds a keyboard handler to switch between two triangle pipeline.

## 3. Drawing meshes

- [TriangleVertexAttrs](./src/TriangleVertexAttrs.hs) and [Render.TriMesh.Pipeline](./src/Render/TriMesh/Pipeline.hs) draws a triange using vertex buffer.
- [TrianglePushConstants](./src/TrianglePushConstants.hs) and [Render.TriMeshPush.Code](./src/Render/TriMeshPush/Code.hs) animate the triangle using a PushConstant render_matrix.
- [MonkeyMesh](./src/MonkeyMesh.hs) draws a rotating monkey head. Checkout related blog post: [Vulkan triangle winding](https://tristancacqueray.github.io/blog/vulkan-triangle-winding).

## 4. Shader input/output

- [SceneDescSet](./src/SceneDescSet.hs) use Render.DescSets.Set0 to setup the camera.

## 5. Textures

- [TextureShow](./src/TextureShow.hs) draws a static texture.
- [OffscreenRender](./src/OffscreenRender.hs) draw a dynamic texture rendered offscreen.

- [FullscreenShader](./src/FullscreenShader.hs) use a fullscreen shader to render a texture, see: [rendering a fullscreen quad without buffers](https://www.saschawillems.de/blog/2016/08/13/vulkan-tutorial-on-rendering-a-fullscreen-quad-without-buffers/). This demo features a custom DescSets to share a single texture and samplers, see [Render.Fullscreen.Pipeline](./src/Render/Fullscreen/Pipeline.hs).

## Extra

- [ScreenshotTest](./src/ScreenshotTest.hs) demonstrates how to record an offscreen render pass framebuffer to a png file.

- [Mandelbrot](./src/Mandelbrot.hs) combines the RenderDearImGui, OffscreenRender and FullscreenShader examples to render a shadertoy onto a texture:

![mandelbrot](./examples/Mandelbrot.png)
