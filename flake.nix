{
  inputs = {
    hspkgs.url =
      "github:podenv/hspkgs/acb9b59f2dc1cfb11ffe1b1062449c8a5dc2f145";
    geomancy.url = "gitlab:dpwiz/geomancy";
    geomancy.flake = false;
    keid.url = "gitlab:keid/engine";
    keid.flake = false;
  };
  outputs = { self, hspkgs, geomancy, keid }:
    let
      pkgs = hspkgs.pkgs;
      gfx-pkgs = with pkgs; [
        vulkan-headers
        vulkan-tools
        vulkan-loader
        vulkan-validation-layers
        glslang

        xorg.xrandr
        xorg.libXcursor
        xorg.libXxf86vm
        xorg.libXinerama
        xorg.libXrandr
        xorg.libXi
        xorg.libXext
      ];
      add-gfx = drv: pkgs.haskell.lib.addPkgconfigDepends drv gfx-pkgs;

      haskellExtend = hpFinal: hpPrev:
        let
          mkKeidPkg = name:
            hpPrev.callCabal2nix "keid-${name}" "${keid}/${name}" { };
        in {
          keid-vkguide = hpPrev.callCabal2nix "keid-vkguide" self { };
          geomancy = hpPrev.callCabal2nix "geomancy" geomancy { };
          keid-core = mkKeidPkg "core";
          keid-render-basic = add-gfx (mkKeidPkg "render-basic");
          keid-geometry = mkKeidPkg "geometry";
          keid-ui-dearimgui = mkKeidPkg "ui-dearimgui";
          gltf-codec = pkgs.haskell.lib.dontCheck
            (pkgs.haskell.lib.overrideCabal hpPrev.gltf-codec {
              broken = false;
            });
          vulkan-utils = pkgs.haskell.lib.dontCheck hpPrev.vulkan-utils;
          bindings-GLFW = pkgs.haskell.lib.dontCheck
            (pkgs.haskell.lib.addPkgconfigDepends
              (pkgs.haskell.lib.disableCabalFlag
                (pkgs.haskell.lib.enableCabalFlag hpPrev.bindings-GLFW
                  "system-GLFW") "X") [ pkgs.glfw-wayland ]);
          dear-imgui = pkgs.haskell.lib.addPkgconfigDepends
            (pkgs.haskell.lib.addBuildDepends (pkgs.haskell.lib.enableCabalFlag
              (pkgs.haskell.lib.enableCabalFlag hpPrev.dear-imgui "vulkan")
              "glfw") [ hpPrev.vulkan hpPrev.GLFW-b ]) [
                pkgs.vulkan-headers
                pkgs.vulkan-tools
                pkgs.vulkan-loader
                pkgs.vulkan-validation-layers
                pkgs.glfw-wayland
              ];
        };
      hsPkgs = pkgs.hspkgs.extend haskellExtend;

      # ghc = hsPkgs.ghcWithPackages (p: [ p.vulkan ]);
      ghc = hsPkgs.shellFor {
        packages = p: [ p.keid-vkguide ];
        buildInputs = with pkgs;
          [
            pkgs.nixGL.nixVulkanIntel
            hpack
            cabal-install
            ghcid
            haskell-language-server
            pkg-config
            xorg.libXdmcp
          ] ++ gfx-pkgs;
      };

    in { devShell.x86_64-linux = ghc; };
}
