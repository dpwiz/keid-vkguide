module MonkeyMesh (stage, loadMonkey) where

-- The explanations assume that you start from the code of TRianglePushConstants.
import RIO
import RIO.State (gets)

import Engine.Stage.Component qualified as Stage
import Engine.Types qualified as Keid

import Engine.Vulkan.Swapchain qualified as Swapchain
import Render.Basic qualified as Basic
import Render.ForwardMsaa qualified as ForwardMsaa
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.DescSets qualified as DescSets
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Render.Draw qualified as Draw

import Engine.Events qualified as Events
import Engine.StageSwitch (trySwitchStage)
import Engine.Window.Key (Key (..))
import Engine.Window.Key qualified as Key
import Resource.Region qualified as Region
import UnliftIO.Resource qualified as Resource

import Geomancy
import RIO.Vector.Storable qualified as Storable
import Resource.Buffer qualified as Buffer
import Resource.CommandBuffer qualified as CommandBuffer
import Resource.Model qualified as Model

import Data.Tagged (Tagged (unTagged))
import Foreign qualified
import Geomancy.Transform qualified as Transform

import Data.Vector qualified as Vector
import Geomancy.Vec3 qualified as Vec3
import Render.TriMeshPush.Pipeline qualified as TriMeshPush
import Resource.Gltf.Load qualified as GltfLoader
import Resource.Gltf.Model qualified as GltfModel

import Engine.Camera qualified as Camera

data RunState = RunState
    { rsModel :: TriMeshPush.Model 'Buffer.Staged
    , rsInstances :: Buffer.Allocated 'Buffer.Coherent TriMeshPush.InstanceAttrs
    }

data Pipelines = Pipelines
    { trimeshPipeline :: TriMeshPush.Pipeline
    }

type StageFrameRIO a = Keid.StageFrameRIO Basic.RenderPasses Pipelines Stage.NoFrameResources RunState a

stage :: Keid.Stage Basic.RenderPasses Pipelines Stage.NoFrameResources RunState
stage = Stage.assemble "Main" rendering resources (Just scene)
  where
    rendering =
        Basic.rendering_
            { Stage.rAllocateP = \swapchain rps -> do
                Pipelines
                    <$> TriMeshPush.allocate (Swapchain.getMultisample swapchain) rps.rpForwardMsaa
            }

    resources =
        Stage.Resources
            { rInitialRS = initialRunState
            , rInitialRR = \_pool _rp _p -> pure Stage.NoFrameResources
            }

    scene =
        mempty
            { Stage.scRecordCommands = recordCommands
            , Stage.scBeforeLoop =
                void $! Region.local $ Events.spawn handleEvent [Key.callback . keyHandler]
            }

    keyHandler (Events.Sink signal) _ (_, _, key) = case key of
        Key'Escape -> signal ()
        _ -> pure ()

    handleEvent () = void $ trySwitchStage Keid.Finish

initialRunState :: Keid.StageRIO st (Resource.ReleaseKey, RunState)
initialRunState = do
    context <- ask
    CommandBuffer.withPools \pools -> do
        logInfo "Loading model"
        (meshPos, meshAttrs, meshIndices) <- loadMonkey
        let meshAttrs2Vertices (pos, attrs) =
                Model.Vertex pos (TriMeshPush.VertexAttrs attrs.vaNormal)

        rsModel <- Model.createStagedL context pools (meshAttrs2Vertices <$> (zip meshPos meshAttrs)) (Just meshIndices)
        modelKey <- Resource.register $ Model.destroyIndexed context rsModel

        logInfo "Creating the instance"
        let instancesS = Storable.fromList [vec3 0 0 0]
        (instanceKey, rsInstances) <- Buffer.allocateCoherent context Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 1 instancesS

        releaseKeys <- Resource.register $ traverse_ Resource.release [modelKey, instanceKey]
        pure (releaseKeys, RunState{..})

loadMonkey :: HasLogFunc env => RIO env ([Vec3.Packed], [GltfModel.VertexAttrs], [Word32])
loadMonkey = do
    -- See: https://hackage.haskell.org/package/keid-resource-gltf-0.1.0.0/docs/Resource-Gltf-Load.html
    -- and: https://hackage.haskell.org/package/keid-resource-gltf-0.1.0.0/docs/Resource-Gltf-Model.html#t:MeshPrimitive
    let -- reverseIndices is necessary when loading opengl model because vulkan uses different triangle winding
        reverseIndices = True
    (_, meshes) <- GltfLoader.loadMeshPrimitives reverseIndices False "assets/monkey.glb"
    pure $ case Vector.toList meshes of
        [meshPrim] -> case Vector.toList meshPrim of
            [(_, stuff)] -> (Vector.toList stuff.sPositions, Vector.toList stuff.sAttrs, Vector.toList stuff.sIndices)
            _ -> error "Expected a single primitive"
        _ -> error "Expected a single mesh"

recordCommands :: Vk.CommandBuffer -> Stage.NoFrameResources -> Word32 -> StageFrameRIO ()
recordCommands cb _ imageIndex = do
    Keid.Frame{fSwapchainResources, fRenderpass, fPipelines} <- asks snd

    triModel <- gets rsModel
    triInstances <- gets rsInstances

    time <- (/ 2) <$> getMonotonicTime
    let camera =
            Camera.ProjectionInput
                { projectionNear = Camera.PROJECTION_NEAR
                , projectionFar = Camera.PROJECTION_FAR
                , projectionParams = pi / 3
                }
        projection =
            Camera.mkTransformPerspective (Vk.Extent2D 800 600) camera

    let model =
            mconcat
                [ Transform.rotateX -1.2
                , Transform.rotateY (-1 * realToFrac time)
                , Transform.translate 0 0 -5
                ]

    let render_matrix = mconcat [model, projection]
    ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
        Swapchain.setDynamicFullscreen cb fSwapchainResources

        DescSets.withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS (fPipelines.trimeshPipeline.pLayout) mempty do
            Graphics.bind cb fPipelines.trimeshPipeline do
                liftIO $ Foreign.with render_matrix \bufPtr -> do
                    let layout = unTagged fPipelines.trimeshPipeline.pLayout
                        size = fromIntegral $ Foreign.sizeOf render_matrix
                        ptr = Foreign.castPtr bufPtr
                    Vk.cmdPushConstants cb layout Vk.SHADER_STAGE_VERTEX_BIT 0 size ptr

                Draw.indexed cb triModel triInstances
