module TriangleRed where

-- The explanations assume that you start from the code of RenderDearImGui.
import RIO

import Engine.Stage.Component qualified as Stage
import Engine.Types qualified as Keid

import Render.Basic qualified as Basic
import Render.ForwardMsaa qualified as ForwardMsaa
import Vulkan.Core10 qualified as Vk

-- In this chapter, we'll use a custom pipeline
import Engine.Vulkan.Swapchain qualified as Swapchain
import Render.RedTriangle.Pipeline qualified as RedTriangle

-- And we need some extra import to use it manually
import Engine.Vulkan.DescSets qualified as DescSets
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Render.Draw qualified as Draw

newtype Pipelines = Pipelines
    { redtrianglePipeline :: RedTriangle.Pipeline
    }

type StageFrameRIO a = Keid.StageFrameRIO Basic.RenderPasses Pipelines Stage.NoFrameResources Stage.NoRunState a

stage :: Keid.Stage Basic.RenderPasses Pipelines Stage.NoFrameResources Stage.NoRunState
stage = Stage.assemble "Main" rendering Stage.noResources (Just scene)
  where
    -- Here we allocate the new triangle pipeline
    rendering =
        Basic.rendering_
            { Stage.rAllocateP = \swapchain rps -> do
                Pipelines
                    <$> RedTriangle.allocate (Swapchain.getMultisample swapchain) rps.rpForwardMsaa
            }

    scene = mempty{Stage.scRecordCommands = recordCommands}

recordCommands :: Vk.CommandBuffer -> Stage.NoFrameResources -> Word32 -> StageFrameRIO ()
recordCommands cb _ imageIndex = do
    Keid.Frame{fSwapchainResources, fRenderpass, fPipelines} <- asks snd
    ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
        Swapchain.setDynamicFullscreen cb fSwapchainResources

        -- The pipeline has no desc set, but we still needs to bind one, using mempty description
        DescSets.withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS (fPipelines.redtrianglePipeline.pLayout) mempty do
            Graphics.bind cb fPipelines.redtrianglePipeline do
                Draw.triangle_ cb
