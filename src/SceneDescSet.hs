module SceneDescSet (stage) where

-- The explanations assume that you start from the code of MonkeyMesh
import RIO
import RIO.State (gets)

import Engine.Stage.Component qualified as Stage
import Engine.Types qualified as Keid

import Engine.Vulkan.Swapchain qualified as Swapchain
import Render.Basic qualified as Basic
import Render.ForwardMsaa qualified as ForwardMsaa
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Render.Draw qualified as Draw

import Engine.Events qualified as Events
import Engine.StageSwitch (trySwitchStage)
import Engine.Window.Key (Key (..))
import Engine.Window.Key qualified as Key
import Resource.Region qualified as Region
import UnliftIO.Resource qualified as Resource

import Geomancy
import RIO.Vector.Storable qualified as Storable
import Resource.Buffer qualified as Buffer
import Resource.CommandBuffer qualified as CommandBuffer
import Resource.Model qualified as Model

import Geomancy.Transform qualified as Transform

import Resource.Gltf.Model qualified as GltfModel

import Engine.Camera qualified as Camera
import Geomancy.Vulkan.View (lookAt)

-- Keid provides a ready to use DescSets.Set0 for general purpose scene that works with the basic pipeline

import Engine.Worker qualified as Worker
import Geomancy.Vec3 qualified as Vec3
import MonkeyMesh (loadMonkey)
import Render.DescSets.Set0 qualified as Set0
import Render.Unlit.Colored.Model qualified as UnlitColored

data RunState = RunState
    { rsModel :: UnlitColored.Model 'Buffer.Staged
    , rsInstances :: Buffer.Allocated 'Buffer.Coherent UnlitColored.InstanceAttrs
    , -- We store the Set0 cpu data in the RunState
      rsSceneP :: Set0.Process
    }

-- And we store the gpu data in the FrameResources
data FrameResources = FrameResources
    { frScene :: Set0.FrameResource '[Set0.Scene]
    }

mkScene :: Transform -> Camera.Projection 'Camera.Perspective -> Set0.Scene
mkScene staticView Camera.Projection{..} =
    Set0.emptyScene
        { Set0.sceneProjection = projectionTransform
        , Set0.sceneView = staticView
        }

type StageFrameRIO a = Keid.StageFrameRIO Basic.RenderPasses Basic.Pipelines FrameResources RunState a

stage :: Keid.Stage Basic.RenderPasses Basic.Pipelines FrameResources RunState
stage = Stage.assemble "Main" rendering resources (Just scene)
  where
    rendering = Basic.rendering_

    resources =
        Stage.Resources
            { rInitialRS = initialRunState
            , rInitialRR = \_pool _rp pipelines ->
                FrameResources <$> Set0.allocate (pipelines.pSceneLayout) [] [] Nothing mempty Nothing
            }

    scene =
        mempty
            { Stage.scRecordCommands = recordCommands
            , Stage.scUpdateBuffers = \RunState{..} FrameResources{..} -> do
                Set0.observe rsSceneP frScene
            , Stage.scBeforeLoop =
                void $! Region.local $ Events.spawn handleEvent [Key.callback . keyHandler]
            }

    keyHandler (Events.Sink signal) _ (_, _, key) = case key of
        Key'Escape -> signal ()
        _ -> pure ()

    handleEvent () = void $ trySwitchStage Keid.Finish

initialRunState :: Keid.StageRIO st (Resource.ReleaseKey, RunState)
initialRunState = do
    (perspectiveKey, perspective) <- Worker.registered Camera.spawnPerspective
    let staticView = lookAt (vec3 0 0 (-1)) (vec3 0 0 0) (vec3 0 (-1) 0)
    (sceneKey, rsSceneP) <-
        Worker.registered $
            Worker.spawnMerge1 (mkScene staticView) perspective

    context <- ask
    CommandBuffer.withPools \pools -> do
        logInfo "Loading model"

        (meshPos, meshAttrs, meshIndices) <- loadMonkey
        let meshAttrs2Vertices (pos, attrs) =
                let WithVec3 x y z = Vec3.unPacked attrs.vaNormal
                 in Model.Vertex pos (vec4 x y z 1.0)

        let attrs = zip meshPos meshAttrs
        rsModel <- Model.createStagedL context pools (meshAttrs2Vertices <$> attrs) (Just meshIndices)
        modelKey <- Resource.register $ Model.destroyIndexed context rsModel

        logInfo "Creating the instance buffer"
        let instancesS = Storable.fromList [mempty, mempty, mempty]
        (instanceKey, rsInstances) <- Buffer.allocateCoherent context Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 10 instancesS

        releaseKeys <- Resource.register $ traverse_ Resource.release [sceneKey, modelKey, instanceKey, perspectiveKey]
        pure (releaseKeys, RunState{..})

recordCommands :: Vk.CommandBuffer -> FrameResources -> Word32 -> StageFrameRIO ()
recordCommands cb FrameResources{frScene} imageIndex = do
    Keid.Frame{fSwapchainResources, fRenderpass, fPipelines} <- asks snd

    triModel <- gets rsModel
    triInstances <- gets rsInstances

    time <- getMonotonicTime
    let new =
            [ Transform.rotateX (realToFrac time) <> Transform.translate -2.5 0 2
            , Transform.rotateX 1.2 <> Transform.rotateY (realToFrac time) <> Transform.translate 0 0 2
            , Transform.rotateX 1.2 <> Transform.rotateZ (realToFrac time) <> Transform.translate 2.5 0 2
            ]
    void $ Buffer.updateCoherent (Storable.fromList new) triInstances

    ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
        Swapchain.setDynamicFullscreen cb fSwapchainResources

        Set0.withBoundSet0 frScene (fPipelines.pUnlitColored) cb do
            Graphics.bind cb fPipelines.pUnlitColored do
                Draw.indexed cb triModel triInstances
