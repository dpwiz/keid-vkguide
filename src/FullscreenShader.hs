module FullscreenShader where

import Data.ByteString qualified as BS
import Data.Vector qualified as Vector
import RIO

import Engine.Stage.Component qualified as Stage
import Engine.Types qualified as Keid
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Vulkan.Types (RenderPass (..))
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Render.Draw qualified as Draw
import Render.Samplers qualified as Samplers

import Engine.Events qualified as Events
import Engine.StageSwitch (trySwitchStage)
import Engine.Window.Key (Key (..))
import Engine.Window.Key qualified as Key
import Resource.Region qualified as Region
import Resource.Texture qualified as Texture
import Resource.Texture.Ktx1 qualified as Ktx1

import Render.ForwardMsaa (ForwardMsaa)
import Render.ForwardMsaa qualified as ForwardMsaa
import Render.Fullscreen.Pipeline qualified as Fullscreen

newtype Pipelines = Pipelines
    { pFullscreen :: Fullscreen.Pipeline
    }

newtype RenderPasses = RenderPasses
    { rpForwardMsaa :: ForwardMsaa
    }

newtype FrameResources = FrameResources
    { frScene :: Fullscreen.FrameResource '[Fullscreen.FullscreenTexture]
    }

instance RenderPass RenderPasses where
    updateRenderpass swapchain RenderPasses{..} =
        RenderPasses
            <$> ForwardMsaa.updateMsaa swapchain rpForwardMsaa
    refcountRenderpass RenderPasses{..} = do
        refcountRenderpass rpForwardMsaa

-- Engine types
type StageFrameRIO a = Keid.StageFrameRIO RenderPasses Pipelines FrameResources Stage.NoRunState a

stage :: Keid.Stage RenderPasses Pipelines FrameResources Stage.NoRunState
stage = Stage.assemble "Main" rendering resources (Just scene)
  where
    rendering =
        Stage.Rendering
            { Stage.rAllocateRP = \swapchain -> do
                RenderPasses <$> ForwardMsaa.allocateMsaa swapchain
            , Stage.rAllocateP = \swapchain rps -> do
                (_, samplers) <- Samplers.allocate (Swapchain.getAnisotropy swapchain)
                let ds = Fullscreen.mkBindings samplers
                Pipelines
                    <$> Fullscreen.allocate (Swapchain.getMultisample swapchain) ds rps.rpForwardMsaa
            }

    resources =
        Stage.noResources
            { Stage.rInitialRR = \pool _rp p -> do
                textureBytes <- liftIO $ BS.readFile "assets/david.ktx"
                (_textureKey, texture) <- Texture.allocateTextureWith (Ktx1.loadBytes pool) textureBytes
                FrameResources
                    <$> Fullscreen.allocateFR (Vector.head <$> p.pFullscreen.pDescLayouts) texture
            }

    scene =
        mempty
            { Stage.scRecordCommands = recordCommands
            , Stage.scBeforeLoop =
                void $! Region.local $ Events.spawn handleEvent [Key.callback . keyHandler]
            }

    keyHandler (Events.Sink signal) _ (_, _, key) = case key of
        Key'Escape -> signal ()
        _ -> pure ()

    handleEvent () = void $ trySwitchStage Keid.Finish

recordCommands :: Vk.CommandBuffer -> FrameResources -> Word32 -> StageFrameRIO ()
recordCommands cb fr imageIndex = do
    Keid.Frame{fSwapchainResources, fRenderpass, fPipelines} <- asks snd

    ForwardMsaa.usePass fRenderpass.rpForwardMsaa imageIndex cb do
        Swapchain.setDynamicFullscreen cb fSwapchainResources

        Fullscreen.withBoundSet0 fr.frScene fPipelines.pFullscreen cb do
            Graphics.bind cb fPipelines.pFullscreen do
                Draw.triangle_ cb
