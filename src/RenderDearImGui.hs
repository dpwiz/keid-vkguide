module RenderDearImGui where

-- The explanations assume that you start from the code of InitialSetup.
import RIO

import Engine.Stage.Component qualified as Stage
import Engine.Types qualified as Keid

-- The render loop is provided by the render-basic library.

import Render.Basic qualified as Basic
import Render.ForwardMsaa qualified as ForwardMsaa
import Vulkan.Core10 qualified as Vk

-- In this chapter, we'll use dear-imgui to draw something.

import DearImGui qualified
import Render.ImGui qualified as ImGui

-- This stage adds basic render passes and pipelines.
stage :: Keid.Stage Basic.RenderPasses Basic.Pipelines Stage.NoFrameResources Stage.NoRunState
stage = Stage.assemble "Main" rendering Stage.noResources (Just scene)
  where
    -- Here we allocate the imgui context along with the basic rendering pipelines.
    rendering =
        Basic.rendering_
            { Stage.rAllocateP = \swapchain rps -> do
                void $! ImGui.allocate swapchain (Basic.rpForwardMsaa rps) 0
                Basic.allocatePipelines_ swapchain rps
            }

    scene =
        mempty
            { Stage.scBeforeLoop = ImGui.allocateLoop True
            , Stage.scRecordCommands = recordCommands
            }

recordCommands ::
    Vk.CommandBuffer ->
    Stage.NoFrameResources ->
    Word32 ->
    Basic.StageFrameRIO Stage.NoFrameResources Stage.NoRunState ()
recordCommands cb _ imageIndex = do
    -- Here we create the imgui content
    dear <-
        snd <$> ImGui.mkDrawData do
            DearImGui.withWindowOpen "Hello Keid" do
                -- Add test code here:
                -- DearImGui.plotLines "test" [0, 0.2, 0.5, 5, 4, 3, 2, 1]

                DearImGui.text "Warning: Avoiding success may incur some costs."

            DearImGui.showDemoWindow

    -- And we draw them on the current frame
    Keid.Frame{fRenderpass} <- asks snd
    ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
        ImGui.draw dear cb
