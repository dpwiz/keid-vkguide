module TriangleVertexAttrs where

-- The explanations assume that you start from the code of HelloTriangle.
import RIO
import RIO.State (gets)

import Engine.Stage.Component qualified as Stage
import Engine.Types qualified as Keid

import Engine.Vulkan.Swapchain qualified as Swapchain
import Render.Basic qualified as Basic
import Render.ForwardMsaa qualified as ForwardMsaa
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.DescSets qualified as DescSets
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Render.Draw qualified as Draw

import Engine.Events qualified as Events
import Engine.StageSwitch (trySwitchStage)
import Engine.Window.Key (Key (..))
import Engine.Window.Key qualified as Key
import Resource.Region qualified as Region
import UnliftIO.Resource qualified as Resource

-- In this chapter, we'll setup vertex buffer using keid model,
-- See Render.TriMesh.Pipeline

import Render.TriMesh.Pipeline qualified as TriMesh

-- Necessary imports to setup the model:

import Geomancy
import Geomancy.Vec3 qualified as Vec3
import RIO.Vector.Storable qualified as Storable
import Resource.Buffer qualified as Buffer
import Resource.CommandBuffer qualified as CommandBuffer
import Resource.Model qualified as Model

-- The RunState holds the model and the instances.
data RunState = RunState
    { rsModel :: TriMesh.Model 'Buffer.Staged
    , rsInstances :: Buffer.Allocated 'Buffer.Coherent TriMesh.InstanceAttrs
    }

newtype Pipelines = Pipelines
    { trimeshPipeline :: TriMesh.Pipeline
    }

-- Engine types
type StageFrameRIO a = Keid.StageFrameRIO Basic.RenderPasses Pipelines Stage.NoFrameResources RunState a

stage :: Keid.Stage Basic.RenderPasses Pipelines Stage.NoFrameResources RunState
stage = Stage.assemble "Main" rendering resources (Just scene)
  where
    rendering =
        Basic.rendering_
            { Stage.rAllocateP = \swapchain rps -> do
                Pipelines
                    <$> TriMesh.allocate (Swapchain.getMultisample swapchain) rps.rpForwardMsaa
            }

    resources =
        Stage.Resources
            { rInitialRS = initialRunState
            , rInitialRR = \_pool _rp _p -> pure Stage.NoFrameResources
            }

    scene =
        mempty
            { Stage.scRecordCommands = recordCommands
            , Stage.scBeforeLoop =
                void $! Region.local $ Events.spawn handleEvent [Key.callback . keyHandler]
            }

    keyHandler (Events.Sink signal) _ (_, _, key) = case key of
        Key'Escape -> signal ()
        _ -> pure ()

    handleEvent () = void $ trySwitchStage Keid.Finish

initialRunState :: Keid.StageRIO st (Resource.ReleaseKey, RunState)
initialRunState = do
    context <- ask
    CommandBuffer.withPools \pools -> do
        logInfo "Creating model"
        let vertices =
                [ Model.Vertex (Vec3.packed 1 1 0) (TriMesh.VertexAttrs $ Vec3.packed 1 0 0)
                , Model.Vertex (Vec3.packed -1 1 0) (TriMesh.VertexAttrs $ Vec3.packed 0 1 0)
                , Model.Vertex (Vec3.packed 0 -1 0) (TriMesh.VertexAttrs $ Vec3.packed 0 0 1)
                ]
        rsModel <- Model.createStagedL context pools vertices Nothing
        modelKey <- Resource.register $ Model.destroyIndexed context rsModel

        logInfo "Creating the triange instance"
        let instancesS = Storable.fromList [vec3 0 0 0] -- See Note [TriMesh instances].
        (instanceKey, rsInstances) <- Buffer.allocateCoherent context Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 1 instancesS

        releaseKeys <- Resource.register $ traverse_ Resource.release [modelKey, instanceKey]
        pure (releaseKeys, RunState{..})

recordCommands :: Vk.CommandBuffer -> Stage.NoFrameResources -> Word32 -> StageFrameRIO ()
recordCommands cb _ imageIndex = do
    Keid.Frame{fSwapchainResources, fRenderpass, fPipelines} <- asks snd

    -- We retrieve the vertex buffer (model) and the instances from the RunState.
    triModel <- gets rsModel
    triInstances <- gets rsInstances

    ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
        Swapchain.setDynamicFullscreen cb fSwapchainResources

        DescSets.withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS (fPipelines.trimeshPipeline.pLayout) mempty do
            Graphics.bind cb fPipelines.trimeshPipeline do
                -- We draw the instances.
                Draw.indexed cb triModel triInstances
