module ScreenshotTest where

import RIO

import Data.Vector qualified as Vector

import Engine.Stage.Component qualified as Stage
import Engine.StageSwitch (trySwitchStage)
import Engine.Types qualified as Engine
import Engine.Types qualified as Keid

-- rendering
import Engine.Vulkan.Swapchain (setDynamicFullscreen)
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Vulkan.Types (Bound (..), HasSwapchain, RenderPass (..))
import Vulkan.Core10 qualified as Vk

import Render.Samplers qualified as Samplers

import Render.Pass.Offscreen (Offscreen)
import Render.Pass.Offscreen qualified as Offscreen

import Render.ForwardMsaa (ForwardMsaa)
import Render.ForwardMsaa qualified as ForwardMsaa

import Render.ColorUV.Pipeline qualified as ColorUV
import Render.Fullscreen.Pipeline qualified as Fullscreen

import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Render.Draw qualified as Draw

-- events

import Engine.Events qualified as Events
import Engine.Window.Key (Key (..))
import Engine.Window.Key qualified as Key
import Resource.Region qualified as Region

import Render.Pass qualified as Pass
import Resource.Image qualified as Image

-- test screenshot
import DearImGui qualified
import Render.ImGui qualified as ImGui
import Screenshot qualified

data RenderPasses = RenderPasses
    { rpForwardMsaa :: ForwardMsaa
    , rpStatic :: Offscreen
    }
    deriving (Generic)

data Pipelines = Pipelines
    { pFullscreen :: Fullscreen.Pipeline
    , pColorUV :: ColorUV.Pipeline
    }

newtype FrameResources = FrameResources
    { -- The texture used by the fullscreen pipeline
      frTexture :: Fullscreen.FrameResource '[Fullscreen.FullscreenTexture]
    }

type StageFrameRIO a = Keid.StageFrameRIO RenderPasses Pipelines FrameResources Stage.NoRunState a

stage :: Keid.Stage RenderPasses Pipelines FrameResources Stage.NoRunState
stage = Stage.assemble "Main" rendering resources (Just scene)
  where
    rendering =
        Stage.Rendering
            { Stage.rAllocateRP = \swapchain -> do
                rpForwardMsaa <- ForwardMsaa.allocateMsaa swapchain
                rpStatic <- Offscreen.allocate (settingsStatic swapchain)
                pure RenderPasses{..}
            , Stage.rAllocateP = \swapchain rps -> do
                void $! ImGui.allocate swapchain rps.rpForwardMsaa 0
                (_, samplers) <- Samplers.allocate (Swapchain.getAnisotropy swapchain)
                pFullscreen <- Fullscreen.allocate (Swapchain.getMultisample swapchain) (Fullscreen.mkBindings samplers) rps.rpForwardMsaa
                pColorUV <- ColorUV.allocate (settingsStatic swapchain).sSamples rps.rpStatic
                pure Pipelines{..}
            }

    resources =
        Stage.noResources
            { Stage.rInitialRR = \_ rp p -> do
                frTexture <- Fullscreen.allocateFR (Vector.head <$> p.pFullscreen.pDescLayouts) (Offscreen.colorTexture rp.rpStatic)
                pure $ FrameResources{..}
            }

    scene =
        Stage.Scene
            { Stage.scBeforeLoop = do
                ImGui.allocateLoop True
                void $! Region.local $ Events.spawn handleEvent [Key.callback . keyHandler]
            , Stage.scUpdateBuffers = \_ _ -> pure ()
            , Stage.scRecordCommands = recordCommands
            }

    keyHandler (Events.Sink signal) _ (_, _, key) = case key of
        Key'Escape -> signal ()
        _ -> pure ()

    handleEvent () = void $ trySwitchStage Keid.Finish

settingsStatic :: HasSwapchain swapchain => swapchain -> Offscreen.Settings
settingsStatic swapchain =
    Offscreen.Settings
        { sLabel = "Static"
        , sLayers = 1
        , sMultiView = False
        , sSamples = Vk.SAMPLE_COUNT_1_BIT
        , sExtent = Vk.Extent2D 800 600
        , sFormat = Vk.FORMAT_R8G8B8A8_UNORM
        , sDepthFormat = Swapchain.getDepthFormat swapchain
        , sMipMap = False
        }

recordCommands :: Vk.CommandBuffer -> FrameResources -> Word32 -> StageFrameRIO ()
recordCommands cb fr imageIndex = do
    Engine.Frame{fSwapchainResources, fRenderpass, fPipelines} <- asks snd

    dear <-
        snd <$> ImGui.mkDrawData do
            DearImGui.withWindowOpen "keid-vkguide" do
                clicked <- DearImGui.button "screenshot"
                when clicked do
                    let srcImage = fRenderpass.rpStatic.oColor.aiImage
                    Screenshot.recordScreenshotCommands "screenshot.png" srcImage
                    logInfo "Wrote screenshot.png"

    Pass.usePass fRenderpass.rpStatic imageIndex cb do
        let viewport = Offscreen.oRenderArea fRenderpass.rpStatic
            scissor = Offscreen.oRenderArea fRenderpass.rpStatic
        Swapchain.setDynamic cb viewport scissor

        let Bound action = do
                Graphics.bind cb fPipelines.pColorUV do
                    Draw.triangle_ cb
        action

    Pass.usePass fRenderpass.rpForwardMsaa imageIndex cb do
        setDynamicFullscreen cb fSwapchainResources

        Fullscreen.withBoundSet0 fr.frTexture fPipelines.pFullscreen cb do
            Graphics.bind cb fPipelines.pFullscreen do
                Draw.triangle_ cb

        ImGui.draw dear cb

instance RenderPass RenderPasses where
    updateRenderpass swapchain RenderPasses{..} =
        RenderPasses
            <$> ForwardMsaa.updateMsaa swapchain rpForwardMsaa
            <*> pure rpStatic

    refcountRenderpass RenderPasses{..} = do
        refcountRenderpass rpForwardMsaa
        refcountRenderpass rpStatic
