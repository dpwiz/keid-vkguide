module Render.TriMesh.Pipeline (
    Model,
    VertexAttrs (..),
    InstanceAttrs,
    Pipeline,
    allocate,
    Config,
    config,
) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Foreign.Storable.Generic (GStorable)
import Vulkan.Core10 qualified as Vk

import Geomancy
import Geomancy.Vec3 qualified as Vec3
import Resource.Model qualified as Model

import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Types (HasRenderPass (..), HasVulkan)
import Render.Code (compileFrag, compileVert)
import Render.TriMesh.Code qualified as Code

newtype VertexAttrs = VertexAttrs
    { vColor :: Vec3.Packed
    }
    deriving (Generic)

vkVertexAttrs :: [Vk.Format]
vkVertexAttrs =
    [ Vk.FORMAT_R32G32B32_SFLOAT -- vColor :: vec3
    ]

instance GStorable VertexAttrs

-- Note: [Keid Model layout]
-- Model.Indexed create this buffer layout:
--   positions : attrs : instances
--
-- Here we store the position using Vec3.Packed
type Model buf = Model.Indexed buf Vec3.Packed VertexAttrs

vkVertexPos :: [Vk.Format]
vkVertexPos =
    [ Vk.FORMAT_R32G32B32_SFLOAT -- vPosition :: vec3
    ]

type Pipeline = Graphics.Pipeline '[] VertexAttrs InstanceAttrs
type Config = Graphics.Configure Pipeline
type instance Graphics.Specialization Pipeline = ()

-- Note: [TriMesh instances]
-- We don't use instance attr, but we need to provide one, otherwise the Buffer.allocateCoherent call raise VulkanException ERROR_INITIALIZATION_FAILED.
type InstanceAttrs = Vec3

allocate ::
    ( HasVulkan env
    , HasRenderPass renderpass
    ) =>
    Vk.SampleCountFlagBits ->
    renderpass ->
    ResourceT (RIO env) Pipeline
allocate multisample rp = do
    snd <$> Graphics.allocate Nothing multisample config rp

config :: Config
config =
    Graphics.baseConfig
        { Graphics.cStages = Graphics.basicStages vertSpirv fragSpirv
        , Graphics.cVertexInput =
            -- Note: this needs to match the model pos and attr
            Graphics.vertexInput
                [ (Vk.VERTEX_INPUT_RATE_VERTEX, vkVertexPos)
                , -- VertexAttrs
                  (Vk.VERTEX_INPUT_RATE_VERTEX, vkVertexAttrs)
                ]
        }

vertSpirv :: ByteString
vertSpirv = $(compileVert Code.vert)

fragSpirv :: ByteString
fragSpirv = $(compileFrag Code.frag)
