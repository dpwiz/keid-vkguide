module Render.TriMesh.Code (vert, frag) where

import RIO

import Render.Code (Code, glsl)

vert :: Code
vert =
    fromString
        [glsl|
    #version 450

    layout (location = 0) in vec3 vPosition;
    layout (location = 1) in vec3 vColor;

    layout (location = 0) out vec3 outColor;

    void main() {
      gl_Position = vec4(vPosition, 1.0f);
      outColor = vColor;
    }
  |]

frag :: Code
frag =
    fromString
        [glsl|
    #version 450

    layout (location = 0)  in vec3 inColor;

    layout (location = 0) out vec4 outFragColor;

    void main() {
        outFragColor = vec4(inColor, 1.0f);
    }
  |]
