-- |

module Render.ColorUV.Pipeline (
    Pipeline,
    allocate,
    Config,
    config,
) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Types (HasRenderPass (..), HasVulkan)
import Render.Code (compileFrag, compileVert)

import Render.ColorUV.Code qualified as Code

type Pipeline = Graphics.Pipeline '[] () ()
type Config = Graphics.Configure Pipeline
type instance Graphics.Specialization Pipeline = ()

allocate ::
    (HasVulkan env, HasRenderPass renderpass) =>
    Vk.SampleCountFlagBits ->
    renderpass ->
    ResourceT (RIO env) Pipeline
allocate multisample rp =
    snd <$> Graphics.allocate Nothing multisample config rp

config :: Config
config =
    Graphics.baseConfig
        { Graphics.cStages = Graphics.basicStages $(compileVert Code.vert) $(compileFrag Code.frag)
        }
