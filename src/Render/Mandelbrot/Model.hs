module Render.Mandelbrot.Model (
    Scene (..),
    observe,
    mkBindings,
    FrameResource (..),
    set0binding1,
    withBoundSet0,
    allocateFrameResource,
) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Control.Monad.Trans.Resource qualified as ResourceT
import Data.Kind (Type)
import Data.Tagged (Tagged (..))
import Data.Vector qualified as Vector
import Data.Vector.Storable qualified as VectorS
import Foreign.Storable.Generic (GStorable)
import Geomancy
import Vulkan.CStruct.Extends (SomeStruct (..))
import Vulkan.Core10 qualified as Vk
import Vulkan.Zero (Zero (..))

import Engine.Vulkan.DescSets (Bound, withBoundDescriptorSets0)
import Engine.Vulkan.Pipeline (Pipeline)
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Types (DsBindings, HasVulkan (..), MonadVulkan)
import Engine.Worker qualified as Worker
import Resource.Buffer qualified as Buffer
import Resource.DescriptorSet qualified as DescriptorSet

import Render.Code (Code (..), trimming)

-- Note: the attributes order matters, it must match the set0binding0 code.
data Scene = Scene
    { sceneResolution :: IVec2
    , sceneOrigin :: Vec2
    , sceneZoom :: Float
    , sceneMaxIter :: Word32
    , sceneAbsImag :: Bool
    }
    deriving (Show, Generic)

set0binding1 :: Code
set0binding1 =
    Code
        [trimming|
    layout(set=0, binding=1, std140) uniform Globals {
      ivec2 resolution;
      vec2  origin;
      float zoom;
      uint  max_iter;
      bool  abs_imag;
    } scene;
  |]

instance GStorable Scene

-- * Common descriptor set

mkBindings :: Tagged Scene DsBindings
mkBindings =
    Tagged
        [ (set0bind1, zero)
        ]

set0bind1 :: Vk.DescriptorSetLayoutBinding
set0bind1 =
    Vk.DescriptorSetLayoutBinding
        { binding = 1
        , descriptorType = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
        , descriptorCount = 1
        , stageFlags = Vk.SHADER_STAGE_ALL
        , immutableSamplers = mempty
        }

-- * Setup

allocateFrameResource ::
    ( MonadVulkan env m
    ) =>
    Tagged '[Scene] Vk.DescriptorSetLayout ->
    Scene ->
    ResourceT m (FrameResource '[Scene])
allocateFrameResource (Tagged set0layout) initialScene = do
    context <- asks id

    (_dpKey, descPool) <- DescriptorSet.allocatePool 1 dpSizes

    let set0dsCI =
            zero
                { Vk.descriptorPool = descPool
                , Vk.setLayouts = Vector.singleton set0layout
                }
    descSets <-
        (Tagged @'[Scene])
            <$> Vk.allocateDescriptorSets (getDevice context) set0dsCI

    (_, sceneData) <-
        ResourceT.allocate
            (Buffer.createCoherent context Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT 1 $ VectorS.singleton initialScene)
            (Buffer.destroy context)

    updateSet0Ds context descSets sceneData

    scene <- Worker.newObserverIO initialScene

    pure $ FrameResource descSets sceneData scene
  where
    dpSizes :: DescriptorSet.TypeMap Word32
    dpSizes = [(Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1)]

    updateSet0Ds ::
        ( HasVulkan context
        , MonadIO m
        ) =>
        context ->
        Tagged '[Scene] (Vector Vk.DescriptorSet) ->
        Buffer.Allocated 'Buffer.Coherent Scene ->
        m ()
    updateSet0Ds context (Tagged ds) sceneData =
        Vk.updateDescriptorSets (getDevice context) writeSets mempty
      where
        destSet0 = case Vector.headM ds of
            Nothing -> error "assert: descriptor sets promised to contain [Scene]"
            Just one -> one

        writeSet0b0 =
            SomeStruct
                zero
                    { Vk.dstSet = destSet0
                    , Vk.dstBinding = 1
                    , Vk.dstArrayElement = 0
                    , Vk.descriptorCount = 1
                    , Vk.descriptorType = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
                    , Vk.bufferInfo = Vector.singleton set0bind0I
                    }
          where
            set0bind0I =
                Vk.DescriptorBufferInfo
                    { Vk.buffer = Buffer.aBuffer sceneData
                    , Vk.offset = 0
                    , Vk.range = Vk.WHOLE_SIZE
                    }

        writeSets =
            Vector.fromList $ pure writeSet0b0

-- * Frame data

data FrameResource (ds :: [Type]) = FrameResource
    { frDescSets :: Tagged ds (Vector Vk.DescriptorSet)
    , frBuffer :: Buffer.Allocated 'Buffer.Coherent Scene
    , frObserver :: Worker.ObserverIO Scene
    }

observe :: MonadUnliftIO m => Worker.Merge Scene -> FrameResource ds -> m ()
observe process FrameResource{frBuffer, frObserver} =
    Worker.observeIO_ process frObserver \_old new -> do
        _same <- Buffer.updateCoherent (VectorS.singleton new) frBuffer
        pure new

-- * Rendering

withBoundSet0 :: MonadIO m => FrameResource ds -> Pipeline ds vertices instances -> Vk.CommandBuffer -> Bound ds Void Void m b -> m b
withBoundSet0 FrameResource{frDescSets} refPipeline cb =
    withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS refPipeline.pLayout frDescSets
