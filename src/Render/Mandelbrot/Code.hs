-- | The shader code to draw the fractal with the GPU.
--
-- The vertex code simply normalizes the output image into a [0..1] range.
--
-- The fragment code uses the model scene to color the pixels.
module Render.Mandelbrot.Code (vert, frag) where

import RIO

import Render.Code (Code, glsl)
import Render.Mandelbrot.Model (set0binding1)

vert :: Code
vert =
    fromString
        [glsl|
    #version 450

    layout (location = 0) out vec2 outUV;

    void main() {
      // Normalize coordinate from [0..1]
      outUV = vec2((gl_VertexIndex << 1) & 2, gl_VertexIndex & 2);

      // Display full screen
      gl_Position = vec4(outUV * 2.0f - 1.0f, 0.0f, 1.0f);
    }
  |]

frag :: Code
frag =
    fromString
        [glsl|
    #version 450

    ${set0binding1}

    #define MAX_ITER int(scene.max_iter)

    layout (location = 0) in vec2 inUV;
    layout (location = 0) out vec4 oColor;

    vec3 mandelbrot_color(vec2 coord) {
      float idx = 0.0;
      vec2 z = vec2(0.0);
      vec2 c = coord;
      while (idx < MAX_ITER) {
        z = vec2(z.x * z.x - z.y * z.y, 2.0 * z.x * z.y) + c;

        // when abs_imag controller is on:
        if (scene.abs_imag) {
          z.y = abs(z.y);
        }

        if (dot(z, z) > 500.0) {
          break;
        }

        idx = idx + 1;
      }
      float ci;
      if (idx < MAX_ITER) {
        ci = ((idx + 1.0) - log2((0.5 * log2(dot(z, z)))));
        ci = sqrt((ci / 256.0));
      } else {
        return vec3(0);
      }
      return vec3((0.5 + (0.5 * cos(((6.2831 * ci) + 0)))), (0.5 + (0.5 * cos(((6.2831 * ci) + 0.4)))), (0.5 + (0.5 * cos(((6.2831 * ci) + 0.7)))));
    }

    void main() {
      // Adjust position to [-1.0..1.0]
      vec2 uv = (inUV - 0.5) * 2;

      // Take into account the screen ratio
      uv.y = uv.y * -scene.resolution.y / scene.resolution.x;

      // Adjust to the scene center and zoom
      uv = scene.origin + uv * (1.0 / scene.zoom);
      oColor = vec4(mandelbrot_color(uv), 1.0);
    }
  |]
