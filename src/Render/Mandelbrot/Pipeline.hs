module Render.Mandelbrot.Pipeline (
    Pipeline,
    allocate,
    Config,
    config,
) where

import Data.Tagged (Tagged (..))
import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Types (DsBindings, HasRenderPass (..), HasVulkan)
import Render.Code (compileFrag, compileVert)

import Render.Mandelbrot.Code qualified as Code
import Render.Mandelbrot.Model (Scene)

-- The mandelbrot pipeline bindings are tagged with the scene type here
type Pipeline = Graphics.Pipeline '[Scene] () ()
type Config = Graphics.Configure Pipeline
type instance Graphics.Specialization Pipeline = ()

allocate ::
    (HasVulkan env, HasRenderPass renderpass) =>
    Vk.SampleCountFlagBits ->
    Tagged Scene DsBindings ->
    renderpass ->
    ResourceT (RIO env) Pipeline
allocate multisample ds rp =
    snd <$> Graphics.allocate Nothing multisample (config ds) rp

config :: Tagged Scene DsBindings -> Config
config (Tagged ds) =
    Graphics.baseConfig
        { Graphics.cDescLayouts = Tagged @'[Scene] [ds]
        , Graphics.cStages = Graphics.basicStages $(compileVert Code.vert) $(compileFrag Code.frag)
        }
