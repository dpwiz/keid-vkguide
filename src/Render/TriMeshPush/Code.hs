-- This pipeline code just override the previous TriMesh fragment to use a push_constant buffer.
module Render.TriMeshPush.Code (vert, frag) where

import RIO

import Render.Code (Code, glsl)

import Render.TriMesh.Code (frag)

vert :: Code
vert =
    fromString
        [glsl|
    #version 450

    layout (location = 0) in vec3 vPosition;
    layout (location = 1) in vec3 vColor;

    layout (location = 0) out vec3 outColor;

    // push constants block
    layout( push_constant ) uniform constants {
      mat4 render_matrix;
    } PushConstants;

    void main() {
      gl_Position = PushConstants.render_matrix * vec4(vPosition, 1.0f);
      outColor = vColor;
    }
  |]
