module Render.Fullscreen.Pipeline (
    Pipeline,
    allocate,
    Config,
    config,
    mkBindings,
    withBoundSet0,
    FullscreenTexture,
    FrameResource,
    allocateFR,
) where

import Data.Kind (Type)
import Data.Tagged (Tagged (..))
import Data.Vector qualified as Vector
import RIO
import Vulkan.CStruct.Extends (SomeStruct (..))
import Vulkan.Zero (Zero (zero))

import Control.Monad.Trans.Resource (ResourceT)
import Vulkan.Core10 qualified as Vk
import Vulkan.Core12.Promoted_From_VK_EXT_descriptor_indexing qualified as Vk12

import Engine.Vulkan.DescSets qualified as DescSets
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Types (DsBindings, HasRenderPass (..), HasVulkan (getDevice), MonadVulkan)
import Render.Code (compileFrag, compileVert)
import Render.Fullscreen.Code qualified as Code

import Resource.Collection qualified as Collection
import Resource.DescriptorSet qualified as DescriptorSet
import Resource.Image qualified as Image
import Resource.Texture qualified as Texture

type Pipeline = Graphics.Pipeline '[FullscreenTexture] () ()
type Config = Graphics.Configure Pipeline
type instance Graphics.Specialization Pipeline = ()

allocate ::
    ( HasVulkan env
    , HasRenderPass renderpass
    ) =>
    Vk.SampleCountFlagBits ->
    Tagged FullscreenTexture DsBindings ->
    renderpass ->
    ResourceT (RIO env) Pipeline
allocate multisample ds rp =
    snd <$> Graphics.allocate Nothing multisample (config ds) rp

allocateFR ::
    (MonadVulkan env m) =>
    Tagged '[FullscreenTexture] Vk.DescriptorSetLayout ->
    Texture.Texture Texture.Flat ->
    ResourceT m (FrameResource '[FullscreenTexture])
allocateFR (Tagged dsl) texture = do
    context <- asks id
    (_dpKey, descPool) <- DescriptorSet.allocatePool 1 dpSizes
    let set0dsCI =
            zero
                { Vk.descriptorPool = descPool
                , Vk.setLayouts = Vector.singleton dsl
                }
    frDescSets <-
        Tagged @'[FullscreenTexture]
            <$> Vk.allocateDescriptorSets (getDevice context) set0dsCI
    let ds = Vector.head $ unTagged frDescSets
    Vk.updateDescriptorSets (getDevice context) (Vector.singleton $ writeSet0b2 ds) mempty
    pure FrameResource{frDescSets}
  where
    dpSizes =
        [ (Vk.DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1)
        , (Vk.DESCRIPTOR_TYPE_SAMPLER, 8)
        ]

    writeSet0b2 ds =
        SomeStruct
            zero
                { Vk.dstSet = ds
                , Vk.dstBinding = 2
                , Vk.descriptorType = Vk.DESCRIPTOR_TYPE_SAMPLED_IMAGE
                , Vk.dstArrayElement = 0
                , Vk.descriptorCount = 1
                , Vk.imageInfo =
                    Vector.singleton
                        Vk.DescriptorImageInfo
                            { sampler = zero
                            , imageView = Image.aiImageView $ Texture.tAllocatedImage texture
                            , imageLayout = Vk.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
                            }
                }

newtype FrameResource (ds :: [Type]) = FrameResource
    { frDescSets :: Tagged ds (Vector Vk.DescriptorSet)
    }

data FullscreenTexture

mkBindings :: (Foldable samplers) => samplers Vk.Sampler -> Tagged FullscreenTexture DsBindings
mkBindings samplers =
    Tagged
        [ (set0bind1, zero)
        , (set0bind2, Vk12.DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT)
        ]
  where
    set0bind1 =
        Vk.DescriptorSetLayoutBinding
            { Vk.binding = 1
            , Vk.stageFlags = Vk.SHADER_STAGE_FRAGMENT_BIT
            , Vk.descriptorType = Vk.DESCRIPTOR_TYPE_SAMPLER
            , Vk.descriptorCount = fromIntegral $ Vector.length linearSamplers
            , Vk.immutableSamplers = linearSamplers
            }
    linearSamplers = Collection.toVector samplers

    set0bind2 =
        Vk.DescriptorSetLayoutBinding
            { binding = 2
            , descriptorType = Vk.DESCRIPTOR_TYPE_SAMPLED_IMAGE
            , descriptorCount = 1
            , stageFlags = Vk.SHADER_STAGE_FRAGMENT_BIT
            , immutableSamplers = mempty
            }

config :: Tagged FullscreenTexture DsBindings -> Config
config (Tagged ds) =
    Graphics.baseConfig
        { Graphics.cDescLayouts = Tagged @'[FullscreenTexture] [ds]
        , Graphics.cStages = Graphics.basicStages $(compileVert Code.vert) $(compileFrag Code.frag)
        }

withBoundSet0 ::
    MonadIO m =>
    FrameResource ds ->
    Graphics.Pipeline ds vertices instances ->
    Vk.CommandBuffer ->
    DescSets.Bound ds Void Void m b ->
    m b
withBoundSet0 FrameResource{frDescSets} refPipeline cb =
    DescSets.withBoundDescriptorSets0
        cb
        Vk.PIPELINE_BIND_POINT_GRAPHICS
        refPipeline.pLayout
        frDescSets
