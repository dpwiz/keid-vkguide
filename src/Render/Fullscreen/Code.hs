module Render.Fullscreen.Code (vert, frag) where

import RIO

import Render.Code (Code, glsl)

vert :: Code
vert =
    fromString
        [glsl|
    #version 450

    layout (location = 0) out vec2 outUV;

    void main() {
      outUV = vec2((gl_VertexIndex << 1) & 2, gl_VertexIndex & 2);
      gl_Position = vec4(outUV * 2.0f - 1.0f, 0.0f, 1.0f);
    }
  |]

frag :: Code
frag =
    fromString
        [glsl|
    #version 450
    #extension GL_EXT_nonuniform_qualifier : enable

    layout(set=0, binding=1) uniform sampler samplers[];
    layout(set=0, binding=2) uniform texture2D theTexture;

    layout (location = 0) in vec2 inUV;

    layout (location = 0) out vec4 outFragColor;

    void main() {
        // todo: put the samplerid in the set0 and add a dearimgui selector
        int samplerId = 0;
        vec3 color = texture(sampler2D(theTexture, samplers[samplerId]), inUV).xyz;
        outFragColor = vec4(color, 1.0f);
    }
  |]
