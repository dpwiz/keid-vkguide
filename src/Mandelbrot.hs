{- | This example demonstrates:

 * Shadertoy like fullscreen fragment shader to draw a fractal
 * Dear imgui sliders to modify the scene parameters

The fractal is drawn onto a texture using an offscreen renderer.
-}
module Mandelbrot where

import Data.Vector qualified as Vector
import RIO
import RIO.State (gets)

import Geomancy

import Engine.Stage.Component qualified as Stage
import Engine.Types qualified as Keid
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Vulkan.Types (RenderPass (..))
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Render.Draw qualified as Draw
import Render.Samplers qualified as Samplers

import Engine.Events qualified as Events
import Engine.StageSwitch (trySwitchStage)
import Engine.Window.Key (Key (..))
import Engine.Window.Key qualified as Key
import Render.Pass qualified as Pass
import Resource.Region qualified as Region

-- To create texture:
import Render.Pass.Offscreen (Offscreen)
import Render.Pass.Offscreen qualified as Offscreen

-- To draw on screen:
import Render.ForwardMsaa (ForwardMsaa)
import Render.ForwardMsaa qualified as ForwardMsaa

import Render.Fullscreen.Pipeline qualified as Fullscreen
import Render.Mandelbrot.Model qualified as Mandelbrot
import Render.Mandelbrot.Pipeline qualified as Mandelbrot

import DearImGui qualified
import Engine.Types qualified as Engine
import Engine.Worker qualified as Worker
import Render.ImGui qualified as ImGui
import UnliftIO.Resource qualified as Resource

data RenderPasses = RenderPasses
    { -- The forwardMsaa pass renders the final image
      rpForwardMsaa :: ForwardMsaa
    , -- The offscreen pass renders the texture
      rpStatic :: Offscreen
    }

data Pipelines = Pipelines
    { -- The fullscreen pipeline draws the texture on the final image
      pFullscreen :: Fullscreen.Pipeline
    , -- The mandelbrot pipeline draws the fractal on the texture
      pMandelbrot :: Mandelbrot.Pipeline
    }

data FrameResources = FrameResources
    { -- The texture used by the fullscreen pipeline
      frTexture :: Fullscreen.FrameResource '[Fullscreen.FullscreenTexture]
    , -- The parameters used by the mandelbrot pipeline
      frScene :: Mandelbrot.FrameResource '[Mandelbrot.Scene]
    }

-- Engine types
type StageFrameRIO a = Keid.StageFrameRIO RenderPasses Pipelines FrameResources RunState a

stage :: Keid.Stage RenderPasses Pipelines FrameResources RunState
stage = Stage.assemble "Main" rendering resources (Just scene)
  where
    -- The settings for the fractal texture
    settingsStatic swapchain =
        Offscreen.Settings
            { sLabel = "Static"
            , sLayers = 1
            , sMultiView = False
            , sSamples = Vk.SAMPLE_COUNT_1_BIT
            , sExtent = Vk.Extent2D 800 800
            , sFormat = Vk.FORMAT_R8G8B8A8_UNORM
            , sDepthFormat = Swapchain.getDepthFormat swapchain
            , sMipMap = False
            }

    rendering =
        Stage.Rendering
            { Stage.rAllocateRP = \swapchain -> do
                rpForwardMsaa <- ForwardMsaa.allocateMsaa swapchain
                rpStatic <- Offscreen.allocate (settingsStatic swapchain)
                pure RenderPasses{..}
            , Stage.rAllocateP = \swapchain rps -> do
                void $! ImGui.allocate swapchain rps.rpForwardMsaa 0
                (_, samplers) <- Samplers.allocate (Swapchain.getAnisotropy swapchain)
                pFullscreen <- Fullscreen.allocate (Swapchain.getMultisample swapchain) (Fullscreen.mkBindings samplers) rps.rpForwardMsaa
                pMandelbrot <- Mandelbrot.allocate (settingsStatic swapchain).sSamples Mandelbrot.mkBindings rps.rpStatic
                pure Pipelines{..}
            }

    resources =
        Stage.Resources
            { Stage.rInitialRR = \_pool rp p -> do
                -- The final texture is the offscreen output.
                frTexture <- Fullscreen.allocateFR (Vector.head <$> p.pFullscreen.pDescLayouts) (Offscreen.colorTexture rp.rpStatic)
                -- The scene is updated by the scene updateBuffers.
                frScene <- Mandelbrot.allocateFrameResource (Vector.head <$> p.pMandelbrot.pDescLayouts) (mkScene 0)
                pure $ FrameResources{..}
            , Stage.rInitialRS = initialRunState
            }

    scene =
        mempty
            { Stage.scRecordCommands = recordCommands
            , Stage.scUpdateBuffers = \rs fr -> Mandelbrot.observe rs.rsSceneP fr.frScene
            , Stage.scBeforeLoop = do
                ImGui.allocateLoop True
                void $! Region.local $ Events.spawn handleEvent [Key.callback . keyHandler]
            }

    keyHandler (Events.Sink signal) _ (_, _, key) = case key of
        Key'Escape -> signal ()
        _ -> pure ()

    handleEvent () = void $ trySwitchStage Keid.Finish

data RunState = RunState
    { -- The scene managed by dear imgui
      rsScene :: Worker.Var Mandelbrot.Scene
    , -- The final scene, updated when resolution change
      rsSceneP :: Worker.Merge Mandelbrot.Scene
    , -- Trigger to re-render the texture
      rsUpdateP :: Worker.Merge ()
    , rsUpdate :: Worker.ObserverIO ()
    }

mkScene :: IVec2 -> Mandelbrot.Scene
mkScene res =
    Mandelbrot.Scene
        { Mandelbrot.sceneResolution = res
        , Mandelbrot.sceneOrigin = vec2 -1.48 0
        , Mandelbrot.sceneZoom = 0.5
        , Mandelbrot.sceneMaxIter = 42
        , Mandelbrot.sceneAbsImag = False
        }

initialRunState :: Keid.StageSetupRIO (Resource.ReleaseKey, RunState)
initialRunState = do
    -- Create the initial scene
    screen <- Engine.askScreenVar
    currentResolution <- Worker.readVar screen
    rsScene <- Worker.newVar (mkScene (extent2ivec currentResolution))

    -- Keep track of the screen resolution
    (resKey, rsResolutionP) <- Worker.registered $ Worker.spawnMerge1 extent2ivec screen

    -- Update the final scene when the resolution or the params change
    (sceneKey, rsSceneP) <-
        Worker.registered do
            Worker.spawnMerge2 (\r res -> r{Mandelbrot.sceneResolution = res}) rsScene rsResolutionP

    -- Also update the render trigger. TODO: find a simpler solution.
    -- See: sky-playground Stage.Example.Resources.initialRunState
    (updateKey, rsUpdateP) <- Worker.registered $ Worker.spawnMerge1 (const ()) rsSceneP
    rsUpdate <- Worker.newObserverIO ()

    release <-
        Resource.register $
            traverse_ @[]
                Resource.release
                [resKey, sceneKey, updateKey]
    pure (release, RunState{..})
  where
    extent2ivec Vk.Extent2D{width, height} = ivec2 (fromIntegral width) (fromIntegral height)

recordCommands :: Vk.CommandBuffer -> FrameResources -> Word32 -> StageFrameRIO ()
recordCommands cb fr imageIndex = do
    Keid.Frame{fSwapchainResources, fRenderpass, fPipelines} <- asks snd

    scene <- gets rsScene
    dear <-
        snd <$> ImGui.mkDrawData do
            DearImGui.withWindowOpen "Mandelbrot Params" do
                void $!
                    DearImGui.sliderFloat
                        "Zoom"
                        ( Worker.stateVarMap
                            Mandelbrot.sceneZoom
                            (\new params -> params{Mandelbrot.sceneZoom = new})
                            scene
                        )
                        0.5
                        30

                void $!
                    DearImGui.checkbox
                        "Burning ship"
                        ( Worker.stateVarMap
                            Mandelbrot.sceneAbsImag
                            (\new params -> params{Mandelbrot.sceneAbsImag = new})
                            scene
                        )

    -- Render the mandelbrot shader to the offscreen framebuffer
    updater <- gets rsUpdateP
    updateNeeded <- gets rsUpdate
    Worker.observeIO_ updater updateNeeded \() () -> do
        Pass.usePass fRenderpass.rpStatic imageIndex cb do
            let viewport = fRenderpass.rpStatic.oRenderArea
            Swapchain.setDynamic cb viewport viewport

            Mandelbrot.withBoundSet0 fr.frScene fPipelines.pMandelbrot cb do
                Graphics.bind cb fPipelines.pMandelbrot do
                    Draw.triangle_ cb
        logInfo "Rendered the fractal!"

    -- Draw the framebuffer on screen
    Pass.usePass fRenderpass.rpForwardMsaa imageIndex cb do
        Swapchain.setDynamicFullscreen cb fSwapchainResources

        Fullscreen.withBoundSet0 fr.frTexture fPipelines.pFullscreen cb do
            Graphics.bind cb fPipelines.pFullscreen do
                Draw.triangle_ cb

        ImGui.draw dear cb

instance RenderPass RenderPasses where
    updateRenderpass swapchain RenderPasses{..} =
        -- XXX: make sure to also update the refcountRenderpass impl
        RenderPasses
            <$> ForwardMsaa.updateMsaa swapchain rpForwardMsaa
            <*> pure rpStatic
    refcountRenderpass RenderPasses{..} = do
        refcountRenderpass rpForwardMsaa
        refcountRenderpass rpStatic
