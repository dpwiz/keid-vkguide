module VkGuide where

import RIO
import System.Environment (getArgs, withArgs)

import Engine.App (engineMain)
import Engine.Types qualified as Keid

import InitialSetup qualified
import RenderDearImGui qualified

import TriangleColor qualified
import TrianglePushConstants qualified
import TriangleRed qualified
import TriangleVertexAttrs qualified

import MonkeyMesh qualified

import SceneDescSet qualified

import OffscreenRender qualified
import TextureShow qualified

import ScreenshotTest qualified
import FullscreenShader qualified
import Mandelbrot qualified

main :: IO ()
main = do
    getArgs >>= \case
        [] -> error . unlines $ "Available stages:" : map fst examples
        prog : rest ->
            case lookup prog examples of
                Nothing ->
                    error . unlines $
                        ("Unknown stage: " <> show prog)
                            : "Must be one of"
                            : map fst examples
                Just found ->
                    -- Run the stage
                    withArgs ("--size" : "800x600" : "--max-fps" : "30" : rest) $
                        engineMain found
  where
    examples =
        [ ("InitialSetup", Keid.StackStage InitialSetup.stage)
        , ("RenderDearImGui", Keid.StackStage RenderDearImGui.stage)
        , ("TriangleRed", Keid.StackStage TriangleRed.stage)
        , ("TriangleColor", Keid.StackStage TriangleColor.stage)
        , ("TriangleVertexAttrs", Keid.StackStage TriangleVertexAttrs.stage)
        , ("TrianglePushConstants", Keid.StackStage TrianglePushConstants.stage)
        , ("MonkeyMesh", Keid.StackStage MonkeyMesh.stage)
        , ("SceneDescSet", Keid.StackStage SceneDescSet.stage)
        , ("TextureShow", Keid.StackStage TextureShow.stage)
        , ("OffscreenRender", Keid.StackStage OffscreenRender.stage)
        , ("FullscreenShader", Keid.StackStage FullscreenShader.stage)
        , ("ScreenshotTest", Keid.StackStage ScreenshotTest.stage)
        , ("Mandelbrot", Keid.StackStage Mandelbrot.stage)
        ]
